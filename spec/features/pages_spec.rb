# frozen_string_literal: true

require "rails_helper"

class Pages
  include Capybara::DSL

  def visit_home
    visit("/")
  end
end

context "PagesController" do
  let(:pages) { Pages.new }

  feature "Visit homepage" do
    scenario "Welcome text should be present", js: false do
      pages.visit_home
      expect(page).to have_content("Welcome")
    end
  end
end
