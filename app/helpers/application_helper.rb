# frozen_string_literal: true

# Application helpers.
module ApplicationHelper
  def title(base_title: "Pet Hotel", prepend: false, glue: " - ")
    return base_title unless content_for?(:title)
    parts = [content_for(:title), base_title]
    parts.reverse! if prepend
    parts.join(glue)
  end

  def body_classes
    [
      controller_name,
      action_name
    ].join("-")
  end

  def alert_class_for(level)
    case level.to_sym
    when :notice then "info"
    when :success then "success"
    when :error then "error"
    when :alert then "warning"
    end
  end
end
