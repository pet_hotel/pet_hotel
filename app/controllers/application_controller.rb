# frozen_string_literal: true

# Base application controller.
class ApplicationController < ActionController::Base
  include ThemeManagement

  before_action :use_theme_views

  protect_from_forgery with: :reset_session

  protected

  add_flash_types :success, :error
end
