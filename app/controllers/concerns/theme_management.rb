# frozen_string_literal: true

# This module manages theme views.
module ThemeManagement
  extend ActiveSupport::Concern

  DEFAULT_THEMES_PATH = "#{Rails.root}/app/views"
  DEFAULT_THEME       = "theme-default"
  THEME_PREFIX        = "theme-"

  protected

  def use_global_views
    prepend_view_path "#{Rails.root}/app/views/global/"
  end

  def use_theme_views
    use_global_views

    theme_path = File.join(DEFAULT_THEMES_PATH, "theme-flexor")

    if File.directory?(theme_path)
      prepend_view_path theme_path
    else
      prepend_view_path File.join(DEFAULT_THEMES_PATH, DEFAULT_THEME)
    end
  end

  def themes_list
    [
      "theme-default"
    ]
  end

  def theme_options
    themes_list.map { |theme| [theme.gsub(THEME_PREFIX), "", theme] }
  end
end
